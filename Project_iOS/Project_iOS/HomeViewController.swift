//
//  HomeViewController.swift
//  Project_iOS
//
//  Created by Enrique on 1/21/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase



class HomeViewController: UIViewController {

   
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var babysister1ImageView: UIImageView!
    @IBOutlet weak var babysister2ImageView: UIImageView!
    @IBOutlet weak var babysister3ImageView: UIImageView!
    @IBOutlet weak var babysister4ImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var nameLabel2: UILabel!
    @IBOutlet weak var scoreLabel2: UILabel!
    @IBOutlet weak var nameLabel3: UILabel!
    @IBOutlet weak var scoreLabel3: UILabel!
    @IBOutlet weak var nameLabel4: UILabel!
    @IBOutlet weak var scoreLabel4: UILabel!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()

        babysister1ImageView.isUserInteractionEnabled = true
        let singleTab : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(recognizer: )))
        singleTab.numberOfTapsRequired = 1
        babysister1ImageView.addGestureRecognizer(singleTab)
        self.view.addSubview(babysister1ImageView)
        
        babysister2ImageView.isUserInteractionEnabled = true
        let singleTab2 : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture2(recognizer: )))
        singleTab.numberOfTapsRequired = 1
        babysister2ImageView.addGestureRecognizer(singleTab2)
        self.view.addSubview(babysister1ImageView)
        
        babysister3ImageView.isUserInteractionEnabled = true
        let singleTab3 : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture3(recognizer: )))
        singleTab.numberOfTapsRequired = 1
        babysister3ImageView.addGestureRecognizer(singleTab3)
        self.view.addSubview(babysister1ImageView)
        
        babysister4ImageView.isUserInteractionEnabled = true
        let singleTab4 : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture4(recognizer: )))
        singleTab.numberOfTapsRequired = 1
        babysister4ImageView.addGestureRecognizer(singleTab4)
        self.view.addSubview(babysister1ImageView)
        
        // Do any additional setup after loading the view.
        
        let url1 = URL(string: "https://firebasestorage.googleapis.com/v0/b/macbabyios.appspot.com/o/User_icon_2.png?alt=media&token=d5454f43-75a1-4516-afcc-ca03d4ea3cf6")!
        downloadImage(from: url1)
        
        let url2 = URL(string: "https://firebasestorage.googleapis.com/v0/b/macbabyios.appspot.com/o/nin%CC%83era4.jpg?alt=media&token=e315af41-637e-4484-9899-e94b5bf95deb")!
        downloadImage2(from: url2)
        
        let url3 = URL(string: "https://firebasestorage.googleapis.com/v0/b/macbabyios.appspot.com/o/nin%CC%83era3.jpg?alt=media&token=4e2b06bb-d57f-4f6a-b2d0-65926890ed28")!
        downloadImage3(from: url3)
        
        let url4 = URL(string: "https://firebasestorage.googleapis.com/v0/b/macbabyios.appspot.com/o/nin%CC%83era2.jpg?alt=media&token=2b726741-f797-4e1e-af54-91202e17f119")!
        downloadImage4(from: url4)
   
        let ref = Firestore.firestore().collection("babysisters")
        
        ref.document("kkLTaa5KMNkMjsMUHgxh").getDocument { (snapshot, error) in
            
            self.nameLabel.text = snapshot?.data()!["Nombre"] as! String
            
            self.scoreLabel.text = (snapshot?.data()!["Score"] as! String)
            
            let person = PersonData( id:1, name: snapshot?.data()!["Nombre"] as! String, lastName: snapshot?.data()!["Apellido"] as? String, age: snapshot?.data()!["Edad"] as? String, phone: (snapshot?.data()!["Phone"] as! String), email: snapshot?.data()!["Mail"] as? String, status: snapshot?.data()!["Status"] as? Bool, score: snapshot?.data()!["Score"] as? String, img: snapshot?.data()!["Imagen"] as? String)
            
            self.people += [person]
        }
        
        ref.document("eLcdYVFpTFeuEaYsooWm").getDocument { (snapshot, error) in
                        
            self.nameLabel2.text = snapshot?.data()!["Nombre"] as! String
            
            self.scoreLabel2.text = (snapshot?.data()!["Score"] as! String)
            
            let person = PersonData( id:2, name: snapshot?.data()!["Nombre"] as! String, lastName: snapshot?.data()!["Apellido"] as? String, age: snapshot?.data()!["Edad"] as? String, phone: (snapshot?.data()!["Phone"] as! String), email: snapshot?.data()!["Mail"] as? String, status: snapshot?.data()!["Status"] as? Bool, score: snapshot?.data()!["Score"] as? String, img: snapshot?.data()!["Imagen"] as? String)
            
            self.people += [person]
        }
            
        
        ref.document("XW4SmBA9gP0jFJVmEeNr").getDocument { (snapshot, error) in
                               
            self.nameLabel3.text = snapshot?.data()!["Nombre"] as! String
                   
            self.scoreLabel3.text = (snapshot?.data()!["Score"] as! String)
            
            let person = PersonData( id:3, name: snapshot?.data()!["Nombre"] as! String, lastName: snapshot?.data()!["Apellido"] as? String, age: snapshot?.data()!["Edad"] as? String, phone: (snapshot?.data()!["Phone"] as! String), email: snapshot?.data()!["Mail"] as? String, status: snapshot?.data()!["Status"] as? Bool, score: snapshot?.data()!["Score"] as? String, img: snapshot?.data()!["Imagen"] as? String)
            
            self.people += [person]
        }
        
        ref.document("FMlvMhpui6MoTxTkuAv8").getDocument { (snapshot, error) in
                                  
            self.nameLabel4.text = snapshot?.data()!["Nombre"] as! String
                      
            self.scoreLabel4.text = (snapshot?.data()!["Score"] as! String)
            
            let person = PersonData( id:4, name: snapshot?.data()!["Nombre"] as! String, lastName: snapshot?.data()!["Apellido"] as? String, age: snapshot?.data()!["Edad"] as? String, phone: (snapshot?.data()!["Phone"] as! String), email: snapshot?.data()!["Mail"] as? String, status: snapshot?.data()!["Status"] as? Bool, score: snapshot?.data()!["Score"] as? String, img: snapshot?.data()!["Imagen"] as? String)
            
            self.people += [person]
        }
    }
    
    var people : [PersonData] = []
    var selectedPerson:PersonData?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProfileSisterSegue"{
        let profileSister = segue.destination as! BabySisterProfileViewController
            profileSister.sister = selectedPerson
        }
    }
    
    @objc func tapGesture(recognizer: UIGestureRecognizer){
        selectedPerson = people.filter { $0.id == 1 }.first
        self.performSegue(withIdentifier: "ProfileSisterSegue", sender: self)
    }
    
    @objc func tapGesture2(recognizer: UIGestureRecognizer){
        selectedPerson = people.filter { $0.id == 2 }.first
        self.performSegue(withIdentifier: "ProfileSisterSegue", sender: self)
    }
    
    @objc func tapGesture3(recognizer: UIGestureRecognizer){
        selectedPerson = people.filter { $0.id == 3 }.first
        self.performSegue(withIdentifier: "ProfileSisterSegue", sender: self)
    }
    
    @objc func tapGesture4(recognizer: UIGestureRecognizer){
        selectedPerson = people.filter { $0.id == 4 }.first
        self.performSegue(withIdentifier: "ProfileSisterSegue", sender: self)
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()){
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL){
        getData(from: url) { (data, response, error) in
            guard let data = data, error == nil else {return}
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async() {
                self.babysister1ImageView.image = UIImage(data: data)
            }
        }
    }
    
    func downloadImage2(from url: URL){
        getData(from: url) { (data, response, error) in
            guard let data = data, error == nil else {return}
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async() {
                self.babysister2ImageView.image = UIImage(data: data)
            }
        }
    }
    
    func downloadImage3(from url: URL){
        getData(from: url) { (data, response, error) in
            guard let data = data, error == nil else {return}
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async() {
                self.babysister3ImageView.image = UIImage(data: data)
            }
        }
    }
    
    func downloadImage4(from url: URL){
        getData(from: url) { (data, response, error) in
            guard let data = data, error == nil else {return}
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async() {
                self.babysister4ImageView.image = UIImage(data: data)
            }
        }
    }
    
    @IBAction func Tapbabysiter(_ sender: Any) {
        
    }
    
}

struct  PersonData  {
    var id :Int
    var name: String
    var lastName: String?
    var age: String?
    var phone: String?
    var email: String?
    var status: Bool?
    var score: String?
    var img: String?
}
