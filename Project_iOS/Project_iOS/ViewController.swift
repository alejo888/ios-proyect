//
//  ViewController.swift
//  Project_iOS
//
//  Created by Fabricio on 1/14/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func signButton(_ sender: Any) {
        
        self.performSegue(withIdentifier: "SignSegue", sender: self)
    }
    
}

