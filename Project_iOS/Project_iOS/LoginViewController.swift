//
//  LoginViewController.swift
//  Project_iOS
//
//  Created by Fabricio on 1/16/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextFile: UITextField!
    @IBOutlet weak var passwordTextFile: UITextField!
    @IBOutlet weak var loginShapeButton: UIButton!

    @IBOutlet weak var signUPShapeButton: UIButton!
    
    @IBOutlet weak var logoView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //logoView.layer.cornerRadius = self.frame.height / 2
        loginShapeButton.applyDesing()
        signUPShapeButton.applyDesing()
    }
    
    @IBAction func logButton(_ sender: Any) {
        guard let email = emailTextFile.text,
            let password = passwordTextFile.text else{
                return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            print("Result: \(result)")
            print("Error : \(error)")
            
            if error != nil {
                self.presentAlertWith(title: "Error", message: error?.localizedDescription ?? "Ups! an error ocurred")
            }else {
                self.performSegue(withIdentifier: "LogSuccessLogin", sender: self)
            }
        }
        
    }
    
    private func presentAlertWith(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { _ in
            self.emailTextFile.text = ""
            self.passwordTextFile.text = ""
        }
        
        alertController.addAction(okAlertAction)
        present(alertController,animated: true,completion: nil)
    }
    
   
    
    @IBAction func signUpButton(_ sender: Any) {
        self.performSegue(withIdentifier: "segueSignUp", sender: self)
        
    }
    
 
    
    
}


extension UIButton{
    func applyDesing() {
        self.backgroundColor = UIColor(red: 0.86, green: 0.90, blue: 0.96, alpha: 1.0)
        self.layer.cornerRadius = self.frame.height / 2
               // loginShapeButton.layer.shadowColor = UIColor.red.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 3, height:2)
    }
}
