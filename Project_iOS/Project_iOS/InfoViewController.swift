//
//  InfoViewController.swift
//  Project_iOS
//
//  Created by Enrique on 2/14/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import UIKit
import Firebase

class InfoViewController: UIViewController {

    @IBOutlet weak var logOutButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        logOutButton.infoDesing()
        guard let email = Auth.auth().currentUser?.email
                    else{
                        return
                }
                // Do any additional setup after loading the view.
            }
            

            /*
            // MARK: - Navigation

            // In a storyboard-based application, you will often want to do a little preparation before navigation
            override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                // Get the new view controller using segue.destination.
                // Pass the selected object to the new view controller.
            }
            */
            @IBAction func logOutButton(_ sender: Any) {
                do{
                    try Auth.auth().signOut()
                    performSegue(withIdentifier: "homeSegue", sender: sender)
                } catch{
                    print(error)
                }
            }
            
        }

extension UIButton{
    func infoDesing() {
        self.backgroundColor = UIColor(red: 0.86, green: 0.90, blue: 0.96, alpha: 1.0)
        self.layer.cornerRadius = self.frame.height / 2
               // loginShapeButton.layer.shadowColor = UIColor.red.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 3, height:2)
    }
}
