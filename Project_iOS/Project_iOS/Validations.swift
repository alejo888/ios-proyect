//
//  Validations.swift
//  Project_iOS
//
//  Created by Fabricio on 1/16/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import Foundation


class Validation {
    
    static func passwordValidation (_ password: String)-> Bool{
        let passwordTest = NSPredicate(format: "Self Matches %@", "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
}
