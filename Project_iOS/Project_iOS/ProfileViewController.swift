//
//  ProfileViewController.swift
//  Project_iOS
//
//  Created by Enrique on 2/14/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import UIKit
import Firebase


class ProfileViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var email: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = Auth.auth().currentUser
        if let user = user {
            let uid = user.uid
            let email = user.email
            
            let ref = Firestore.firestore().collection("users")
            
            ref.document(uid).getDocument{ (snapshot, error) in
            
                self.username.text = snapshot?.data()!["username"] as! String
                self.phone.text = snapshot?.data()!["phone"] as! String
                self.email.text = snapshot?.data()!["email"] as! String
                
            }
            

            
            
            
            
        }
        
        
        
        
        
        
    }
    

  

}
