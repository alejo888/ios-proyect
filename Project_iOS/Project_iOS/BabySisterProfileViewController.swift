//
//  BabySisterProfileViewController.swift
//  Project_iOS
//
//  Created by Enrique on 1/31/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class BabySisterProfileViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameEditText: UITextField!
    @IBOutlet weak var lastnameEditText: UITextField!
    @IBOutlet weak var ageEditText: UITextField!
    @IBOutlet weak var phoneEditText: UITextField!
    @IBOutlet weak var scoreEditText: UITextField!
    
    @IBOutlet weak var emailEditText: UITextField!
    
    @IBOutlet weak var statusSwitch: UISwitch!
    
    
    var sister: PersonData?
    var url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameEditText.text = sister?.name
        lastnameEditText.text = sister?.lastName
        ageEditText.text = sister?.age
        phoneEditText.text = sister?.phone
        scoreEditText.text = sister?.score
        url = sister?.img
        emailEditText.text = sister?.email
        
        let url1 = URL(string: url!)!
        downloadImage(from: url1)
        
        let boolean1 = sister?.status
        
        dispoSwitch(boolean: boolean1 ?? false)

        // Do any additional setup after loading the view.
    }
    
    func dispoSwitch(boolean: Bool) {
        if let switch1 = statusSwitch {
            
            switch1.isOn = boolean
            
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()){
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL){
        print("Download started")
        getData(from: url) { (data, response, error) in
            guard let data = data, error == nil else {return}
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("download finished")
            DispatchQueue.main.async() {
                self.imgView.image = UIImage(data: data)
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
