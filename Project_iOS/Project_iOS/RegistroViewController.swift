//
//  RegistroViewController.swift
//  Project_iOS
//
//  Created by Fabricio on 1/16/20.
//  Copyright © 2020 Fabricio. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class RegistroViewController: UIViewController {

    @IBOutlet weak var userNameTextFile: UITextField!
    @IBOutlet weak var phoneTextFile: UITextField!
    @IBOutlet weak var emailTextFile: UITextField!
    @IBOutlet weak var passwordTextFile: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.registerDesing()
        cancelButton.registerDesing()
        
    }
    
    func validateFields()-> String?{
        if userNameTextFile.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            phoneTextFile.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextFile.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextFile.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            {
            
            return "ingrese informacion"
        }
        let cleanedPassword = passwordTextFile.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Validation.passwordValidation(cleanedPassword) == false{
            return "Ingrese minimo 8 caracteres, 1 caracter alfabetico y 1 numérico"
        }
        
        return nil
    }
    
    
    @IBAction func saveButton(_ sender: Any) {
        //validar campos
        
        let error = validateFields()
        if error != nil{
            showError(error!)
        }else{
            let userName = userNameTextFile.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let phone = phoneTextFile.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTextFile.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextFile.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            // Create user
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                if err != nil {
                    // error user
                    self.showError("Error creating user")
                }else {
                    // user create
                    let db = Firestore.firestore()
                    
                    db.collection("users").document(result!.user.uid).setData(["username": userName, "phone": phone,"email": email, "password": password, "uid":result!.user.uid], completion: { (error) in
                        if error != nil{
                            self.showError("No se pudo guardar el usuario")
                        }else {
                            self.showError("usuario guardado exitosamente")
                            self.performSegue(withIdentifier: "SaveSuccessRegisterSegue", sender: self)
                            
                        }
                    })
                }

            }
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.performSegue(withIdentifier: "CancelRegisterSegue", sender: self)
    }
    
    
    func showError(_ message:String){
        //errorLabel.text = message
        //errorLabel.alpha = 1
    }
    
}

extension UIButton{
    func registerDesing() {
        self.backgroundColor = UIColor(red: 0.86, green: 0.90, blue: 0.96, alpha: 1.0)
        self.layer.cornerRadius = self.frame.height / 2
        // loginShapeButton.layer.shadowColor = UIColor.red.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 3, height:2)
        
    }
}
